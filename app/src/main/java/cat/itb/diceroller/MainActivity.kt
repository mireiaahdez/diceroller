package cat.itb.diceroller

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Gravity
import android.widget.Button
import android.widget.ImageButton
import android.widget.TextView
import android.widget.Toast

class MainActivity : AppCompatActivity() {
    lateinit var rollButton: Button
    lateinit var resetButton: Button
    lateinit var dice1: ImageButton
    lateinit var dice2: ImageButton


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        rollButton = findViewById(R.id.roll_button)
        resetButton = findViewById(R.id.reset_button)

        dice1 = findViewById(R.id.dice_number)
        dice2 = findViewById(R.id.dice_number2)



        rollButton.setOnClickListener {
            val rnd = (1..6).random()
            val rnd2 = (1..6).random()

            when (rnd) {
                1 -> dice1.setImageResource(R.drawable.dice_1)
                2 -> dice1.setImageResource(R.drawable.dice_2)
                3 -> dice1.setImageResource(R.drawable.dice_3)
                4 -> dice1.setImageResource(R.drawable.dice_4)
                5 -> dice1.setImageResource(R.drawable.dice_5)
                6 -> dice1.setImageResource(R.drawable.dice_6)
            }
            when (rnd2) {
                1 -> dice2.setImageResource(R.drawable.dice_1)
                2 -> dice2.setImageResource(R.drawable.dice_2)
                3 -> dice2.setImageResource(R.drawable.dice_3)
                4 -> dice2.setImageResource(R.drawable.dice_4)
                5 -> dice2.setImageResource(R.drawable.dice_5)
                6 -> dice2.setImageResource(R.drawable.dice_6)
            }


        }
    }
}


